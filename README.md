# RTV1 - Basic Ray Tracer. 

### Language of development: C
 This is a learning project of Unit Factory coding school, which using 
 'ecole 42' franchise and same learning programm as the network of innovative
 schools across the world in such countries as USA, France, Netherlands.
 
 This mini-project is the first step to create a Raytracing program. 
 Executable getting scene data from the file. I have created specific format and
 syntax to describe properties of the objects which will be rendered.
 Data from file parsed and checked for errors. Basic scenes and syntax with
 with description you can check on scenes folder.
 
 To make life funnier, using the external libraries or frameworks was
 forbidden or very limited for student projects. Libft included in repository
 was developed by author as another lerning project in school 42. Available 
 external functions for this project is: open, read, write, close, malloc, free,
 perror, strerror, exit and functions from the math library.
 Also for graphic-specific projects we using MinilibX library which had ability 
 to initialise window, process keys events and output the image to the window.
 All additional stuff associated with images generating/processing, parsing 
 data, etc. student must done by yourself.
 
 NOTE: Project was developed and tested on macOS High Sierra 10.13.3.
 Unfortunately I have no ability to test it on other platforms for now.
 
## Installation
Use make to compile the project.

```make rtv1```
## Usage
Programm is getting the input from file with *.42 extension. 
You can use files from scenes repository or create your own.

Execute with: 
```./rtv1 scenes/filename```

## Interface screenshots
![Screenshot](screenshots/main.png)
### Controls menus
User have ability to change camera position and moreover, objects parameters 
in live mode. Interactive mode activates when mouse-clicking on the object.
![Screenshot](screenshots/controlls.png)
### Specular turned-off
![Screenshot](screenshots/specular_off.png)
### Shadows turned-off
![Screenshot](screenshots/shadows_off.png)
### Errors handling
![Screenshot](screenshots/errors_handling.png)