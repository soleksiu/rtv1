/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_ptr_while_number.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 19:10:31 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/16 19:10:32 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_move_ptr_while_nbr(char **str)
{
	if (str && *str)
		while (ft_isdigit(**str) || *str[0] == '-')
			(*str)++;
}
