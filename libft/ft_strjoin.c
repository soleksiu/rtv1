/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 20:01:20 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/08 17:03:12 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*n;

	if (!s1 || !s2)
		return (NULL);
	n = (char *)malloc(sizeof(char) * (ft_strlen(s1) + (ft_strlen(s2) + 1)));
	if (n)
	{
		ft_strcpy(n, s1);
		ft_strcat(n, s2);
		return (n);
	}
	return (NULL);
}
