/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 21:12:46 by soleksiu          #+#    #+#             */
/*   Updated: 2017/11/09 16:59:22 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*p;
	char	*buff;

	p = (char *)s;
	buff = 0;
	if (c == 0 && *s)
		return (p + (ft_strlen(s)));
	while (*p)
	{
		if (*p == c)
			buff = p;
		p++;
	}
	return (buff);
}
