/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 12:22:09 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/24 12:22:31 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_H
# define RTV1_H
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <mlx.h>
# include <pthread.h>
# include <float.h>
# include "libft.h"

# define M_WIDTH   800
# define M_HEIGHT  500
# define THREADS_NBR  10
# define ANGLE_TO_RAD M_PI/180
# define FIG_NBR  4
# define KEY_A     0
# define KEY_S     1
# define KEY_D     2
# define KEY_W     13
# define KEY_Q     12
# define KEY_E     14
# define KEY_R     15
# define KEY_M     46
# define KEY_UP    126
# define KEY_DOWN  125
# define KEY_LEFT  123
# define KEY_RIGHT 124
# define KEY_PGUP  116
# define KEY_PGDWN 121
# define KEY_SPACE 49
# define KEY_PLUS  24
# define KEY_MINUS 27
# define KEY_LARR  43
# define KEY_RARR  47
# define KEY_HOME  115
# define KEY_END   119
# define KEY_C     8
# define KEY_V     9
# define KEY_K     40
# define KEY_L     37
# define KEY_O     31
# define KEY_P     35
# define KEY_ESC   53
# define KEYS_NBR  28

typedef enum		e_objnames
{
	sphere = 1,
	cone = 2,
	cylinder = 3,
	plane = 4,
	triangle = 5,
	torus = 6,
	skybox = 7,
	light = 8
}					t_objnames;

typedef struct		s_objn
{
	t_objnames		val;
	const char		*obj_name[8];

}					t_objn;

typedef struct		s_coord
{
	double			x_b;
	double			y_b;
	double			x_e;
	double			y_e;
	int				i;
}					t_coord;

typedef struct		s_vect
{
	double			x;
	double			y;
	double			z;

}					t_vect;

typedef struct		s_roots
{
	double			r1;
	double			r2;

}					t_roots;

enum				e_cmp
{
	alpha = 3,
	red = 2,
	green = 1,
	blue = 0

};

typedef union		u_color
{

	unsigned int	color;
	unsigned char	rgb[4];
}					t_color;

typedef struct		s_plane
{
	t_vect			center;
	t_vect			normal;
}					t_plane;

typedef struct		s_sphere
{
	t_vect			center;
	double			radius;
}					t_sphere;

typedef struct		s_cone
{
	t_vect			center;
	t_vect			v;
	double			angle;
	int				cut;
}					t_cone;

typedef struct		s_cylinder
{
	t_vect			center;
	t_vect			v;
	double			radius;

}					t_cylinder;

typedef struct		s_ray
{
	t_vect			start;
	t_vect			direction;
}					t_ray;

typedef struct		s_light
{
	t_vect			pos;
	double			intens;
}					t_light;

typedef struct		s_rtv1
{
	char			*img_addr;
	int				window_width;
	int				window_height;
	int				img_bpp;
	int				img_size_line;
	int				img_endian;
	unsigned int	color;
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	int				flag_c;
	int				flag_d;
	int				flag_h;
	int				flag_a;
	int				flag_i;
	int				flag_m;
	int				first_draw;
	unsigned int	default_color;
	unsigned int	flag_color;
	int				menu;
	int				*keys_arr;
	int				mouse_lk;
	int				mouse_x;
	int				mouse_y;
	double			move_x;
	double			move_y;

	char			*file_path;

	char			*line;
	double			zoom;
	int				fd;
	t_list			*lst;
	unsigned int	objcts_nbr;
	unsigned int	src_nbr;
	void			**scene;
	void			**light_srcs;
	double			ambient_light;
	t_vect			cam;
	double			alt;
	double			rotate_x;
	double			rotate_y;
	double			rotate_z;
	void			*selected_obj;
	int				shadows;
	int				shine;
}					t_rtv1;

typedef struct		s_coef
{
	double			a;
	double			b;
	double			c;
}					t_coef;

typedef	struct		s_thread
{
	char			*img_addr;
	int				iter_nb;
	int				window_width;
	int				window_height;
	int				img_bpp;
	int				img_size_line;
	int				img_endian;
	unsigned int	color;
	int				begin_x;
	int				begin_y;
	unsigned int	flag_color;
	void			**scene;
	void			**light_srcs;
	double			ambient_light;
	unsigned int	src_nbr;
	unsigned int	objcts_nbr;
	double			zoom;
	double			t;
	double			move_x;
	double			move_y;
	double			alt;
	t_vect			cam;
	t_ray			ray;
	double			t_min;
	double			rotate_x;
	double			rotate_y;
	double			rotate_z;
	double			dx;
	double			dy;
	double			dz;
	double			radius_change;
	t_vect			p;
	t_vect			n;
	int				shadows;
	int				shine;
}					t_thread;

typedef struct		s_objct
{
	t_objnames		type;
	void			*addr;
	int				(*ft_intersect)(void *obj, t_ray ray, double *t);
	t_color			color;
	int				spec;
	double			dx;
	double			dy;
	double			dz;
	double			rot_x;
	double			rot_y;
	double			rot_z;
	double			radius_change;
	int				selected;
}					t_objct;

void				ft_create_scene(t_rtv1 *data);
void				ft_open_and_read_file(t_rtv1 *data);
unsigned int		ft_parse_color(char *line, t_rtv1 *data);
unsigned int		ft_get_color(unsigned int color, double coef);
void				ft_keys_manager(t_rtv1 *data);
void				ft_print_controls_menu(t_rtv1 *data);
void				ft_draw_image(t_rtv1 *data);
void				*ft_ray_trace(void *d);
double				ft_calc_light(t_thread *d, t_objct *o);
t_roots				ft_solve_quadratic_equation(double a, double b, double c);

void				ft_create_plane(char *l, t_objct *o_ad, t_rtv1 *data);
void				ft_create_sphere(char *l, t_objct *o_ad, t_rtv1 *data);
void				ft_create_cylinder(char *l, t_objct *o_ad, t_rtv1 *data);
void				ft_create_cone(char *l, t_objct *o_ad, t_rtv1 *data);
void				ft_create_light_source(char *l, t_objct *ad, t_rtv1 *data);

int					ft_intersect_plane(void *obj, t_ray ray, double *t);
int					ft_intersect_sphere(void *obj, t_ray ray, double *t);
int					ft_intersect_cone(void *obj, t_ray ray, double *t);
int					ft_intersect_cylinder(void *obj, t_ray ray, double *t);

void				ft_put_pixel_img(t_thread *d, int x, int y, unsigned int c);
void				ft_error(char *str, t_rtv1 *data);
void				ft_display_help(t_rtv1 *data);
int					ft_check_key_code(int key_nb, t_rtv1 *data);
void				ft_convert_coords(double *x, double *y, double w, double h);
t_vect				ft_rotate_point(t_vect *p, double rx, double ry, double rz);
t_objct				*ft_intersect_objects(t_thread *data, t_coord *c);
void				ft_init_thread_params(t_rtv1 *d, t_thread *th_data, int c);

t_vect				ft_find_hit_point(t_ray *ray, double t);
t_vect				ft_find_normal(t_objct *o, t_vect *p, t_ray *r, double t);

double				ft_vector_length(t_vect *vector);
t_vect				ft_vector_negation(t_vect *vector);
t_vect				ft_vector_addition(t_vect *vector1, t_vect *vector2);
t_vect				ft_vector_subtraction(t_vect *vector1, t_vect *vector2);
t_vect				ft_vector_mult_scalar(t_vect *vector, double scalar);
t_vect				ft_vector_normalizing(t_vect *vector);
t_vect				ft_vector_cross_product(t_vect *vector1, t_vect *vector2);
double				ft_vector_dot_product(t_vect *vector1, t_vect *vector2);
void				ft_output_final_image(t_rtv1 *data);
#endif
