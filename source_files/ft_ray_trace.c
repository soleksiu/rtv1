/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ray_trace.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 16:21:07 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/31 16:23:03 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_set_start_params(t_thread *data, t_coord *c)
{
	c->x_b = data->begin_x;
	c->y_b = data->begin_y;
	c->x_e = data->window_width;
	c->y_e = c->y_b + data->dy;
	c->i = 0;
}

void		*ft_ray_trace(void *d)
{
	size_t			color;
	t_coord			c;
	t_thread		*data;
	t_objct			*obj_intersected;
	double			light_coef;

	data = (t_thread *)d;
	ft_set_start_params(data, &c);
	while (c.y_b < c.y_e)
	{
		while (c.x_b < c.x_e)
		{
			obj_intersected = ft_intersect_objects(data, &c);
			if (obj_intersected &&
				(light_coef = ft_calc_light(data, obj_intersected)))
				color = ft_get_color(obj_intersected->color.color, light_coef);
			else
				color = 0;
			ft_put_pixel_img(data, c.x_b, c.y_b, color);
			(c.x_b)++;
		}
		c.x_b = 0;
		(c.y_b)++;
	}
	return (NULL);
}
