/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intersect_sphere.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 12:45:19 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/24 12:45:21 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_check_roots(t_roots *roots, double *t)
{
	double tmp;

	if (roots->r1 == NAN && roots->r2 == NAN)
		return (0);
	else if (roots->r1 == NAN)
		tmp = roots->r2;
	else if (roots->r2 == NAN)
		tmp = roots->r1;
	else
	{
		if (roots->r1 < 1)
			tmp = roots->r2;
		else if (roots->r2 < 1)
			tmp = roots->r1;
		else
			tmp = roots->r1 < roots->r2 ? roots->r1 : roots->r2;
	}
	if (tmp < 1)
		return (0);
	*t = tmp;
	return (1);
}

static void	ft_change_sphere_params(t_objct *obj, t_sphere *sphere)
{
	sphere->center.x += obj->dx;
	sphere->center.y += obj->dy;
	sphere->center.z += obj->dz;
	sphere->radius += obj->radius_change;
	if (sphere->radius < 1)
		sphere->radius = 1;
	obj->dx = 0;
	obj->dy = 0;
	obj->dz = 0;
	obj->radius_change = 0;
}

int			ft_intersect_sphere(void *s, t_ray ray, double *t)
{
	t_sphere	*sphere;
	t_vect		dist;
	t_coef		coef;
	t_roots		roots;

	sphere = ((t_sphere *)((t_objct*)s)->addr);
	if (((t_objct*)s)->selected)
		ft_change_sphere_params(s, sphere);
	dist = ft_vector_subtraction(&ray.start, &sphere->center);
	coef.a = ft_vector_dot_product(&ray.direction, &ray.direction);
	coef.b = 2 * ft_vector_dot_product(&ray.direction, &dist);
	coef.c = ft_vector_dot_product(&dist, &dist) -
	(sphere->radius * sphere->radius);
	roots = ft_solve_quadratic_equation(coef.a, coef.b, coef.c);
	if (!ft_check_roots(&roots, t))
		return (0);
	return (1);
}
