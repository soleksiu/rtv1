/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_hit_point.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 19:16:28 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/12 19:16:30 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vect	ft_find_hit_point(t_ray *ray, double t)
{
	t_vect	hit_point;

	hit_point = ft_vector_mult_scalar(&ray->direction, t);
	hit_point = ft_vector_addition(&ray->start, &hit_point);
	return (hit_point);
}
