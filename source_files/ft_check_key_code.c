/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_key_code.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:04:52 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:04:55 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static	int	ft_is_supported_key(t_rtv1 *data, int key_code)
{
	int i;

	i = 0;
	while (i < KEYS_NBR)
		if (data->keys_arr[i++] == key_code)
			return (1);
	return (0);
}

static void	ft_keys_wasdqer(t_rtv1 *data, int key_code)
{
	if (key_code == KEY_W || key_code == KEY_S)
	{
		data->zoom += key_code == KEY_W ? 35 : -35;
	}
	else if (key_code == KEY_A || key_code == KEY_D)
		data->move_x += key_code == KEY_D ? 12 : -12;
	else if ((key_code == KEY_E || key_code == KEY_Q))
	{
		data->rotate_z += key_code == KEY_E ? 1.5 : -1.5;
	}
	else if (key_code == KEY_R)
	{
		data->rotate_x = 0;
		data->rotate_y = 0;
		data->rotate_z = 0;
		data->move_x = 0;
		data->move_y = 0;
		data->zoom = 1;
	}
}

static void	ft_keys_udlr_pup_pdwn(t_rtv1 *data, int key_code)
{
	if (key_code == KEY_HOME || key_code == KEY_END)
	{
		data->rotate_x += key_code == KEY_END ? 1.5 : -1.5;
	}
	else if (key_code == KEY_LARR || key_code == KEY_RARR)
	{
		data->rotate_y += key_code == KEY_LARR ? 1.5 : -1.5;
	}
	else if (key_code == KEY_C || key_code == KEY_V)
		data->move_y += key_code == KEY_C ? 10 : -10;
}

static void	ft_keys_for_object_interaction(t_rtv1 *data, int key_code)
{
	t_objct *selected_obj;

	selected_obj = (t_objct *)(data->selected_obj);
	if (!selected_obj)
		return ;
	if (key_code == KEY_PLUS || key_code == KEY_MINUS)
		selected_obj->radius_change += key_code == KEY_PLUS ? 5 : -5;
	else if (key_code == KEY_LEFT || key_code == KEY_RIGHT)
		selected_obj->dx += key_code == KEY_RIGHT ? 10 : -10;
	else if (key_code == KEY_PGDWN || key_code == KEY_PGUP)
		selected_obj->dy += key_code == KEY_PGUP ? 10 : -10;
	else if (key_code == KEY_DOWN || key_code == KEY_UP)
		selected_obj->dz += key_code == KEY_UP ? 12 : -12;
	else if (key_code == KEY_K || key_code == KEY_L)
		selected_obj->rot_z += key_code == KEY_K ? 5 : -5;
}

int			ft_check_key_code(int k_n, t_rtv1 *data)
{
	if (!ft_is_supported_key(data, k_n))
		return (1);
	if (k_n == KEY_ESC)
		exit(0);
	if (k_n == KEY_M)
		data->menu = data->menu == 1 ? 0 : 1;
	if (k_n == KEY_O)
		data->shine = data->shine == 1 ? 0 : 1;
	if (k_n == KEY_P)
		data->shadows = data->shadows == 1 ? 0 : 1;
	if (k_n == KEY_SPACE && data->selected_obj)
	{
		((t_objct *)(data->selected_obj))->selected = 0;
		data->selected_obj = NULL;
	}
	ft_keys_wasdqer(data, k_n);
	ft_keys_for_object_interaction(data, k_n);
	ft_keys_udlr_pup_pdwn(data, k_n);
	mlx_clear_window(data->mlx_ptr, data->win_ptr);
	ft_draw_image(data);
	return (1);
}
