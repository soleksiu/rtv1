/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intersect_plane.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 12:43:59 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/24 12:44:00 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_change_plane_params(t_objct *obj, t_plane *plane)
{
	plane->center.z += obj->dz;
	if (obj->rot_z)
	{
		plane->normal = ft_rotate_point(&plane->normal, obj->rot_z, 0, 0);
	}
	obj->rot_z = 0;
	obj->dz = 0;
}

/*
** main equasion t = -X|V / D|V;
** where X is dist from camera to plane center
** V is plane normal
** | is dot product of vectors
*/

int			ft_intersect_plane(void *p, t_ray ray, double *t)
{
	t_plane		*plane;
	t_vect		dist;
	double		x_v;
	double		d_v;
	double		res;

	plane = ((t_plane *)((t_objct*)p)->addr);
	if (((t_objct*)p)->selected)
		ft_change_plane_params(p, plane);
	dist = ft_vector_subtraction(&ray.start, &plane->center);
	x_v = ft_vector_dot_product(&dist, &plane->normal);
	d_v = ft_vector_dot_product(&ray.direction, &plane->normal);
	if (!d_v)
		return (0);
	res = (-x_v) / d_v;
	if (res < 0)
		return (0);
	else
		*t = res;
	return (1);
}
