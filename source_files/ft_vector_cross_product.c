/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_cross_product.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 15:44:49 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/25 15:47:35 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vect	ft_vector_cross_product(t_vect *vector1, t_vect *vector2)
{
	t_vect	result;

	result.x = vector1->y * vector2->z - vector1->z * vector2->y;
	result.y = vector1->z * vector2->x - vector1->x * vector2->z;
	result.z = vector1->x * vector2->y - vector1->y * vector2->x;
	return (result);
}
