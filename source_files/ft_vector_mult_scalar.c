/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_mult_scalar.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 15:02:27 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/25 15:03:56 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vect	ft_vector_mult_scalar(t_vect *vector, double scalar)
{
	t_vect	result;

	result.x = vector->x * scalar;
	result.y = vector->y * scalar;
	result.z = vector->z * scalar;
	return (result);
}
