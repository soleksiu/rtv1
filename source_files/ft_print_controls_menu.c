/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_controls_menu.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:09:36 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:09:40 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"
#define CON  "CONTROLS MENU"
#define W    "W . . . . . move forward"
#define S    "S . . . . . move backward"
#define D    "D . . . . . move right"
#define A    "A . . . . . move left"
#define R_Z  "E/Q . . . . rotate over z-axis"
#define R_Y  "<>  . . . . rotate over y-axis"
#define R_X  "HOME/END  . rotate over x-axis"
#define C_V  "C/V . . . . change altitude"
#define O    "O . . . . . turn on/off shine"
#define P    "P . . . . . turn on/off shadows"

#define OBJ  "OBJECTS MODYFYING:"
#define UP   "UP  . . . . move forward"
#define DOWN "DOWN  . . . move backward"
#define LEFT "LEFT  . . . move left"
#define RGHT "RIGHT . . . move right"
#define ALT  "PUP/PDWN  . move along y-axis"
#define ROT  "K/L . . . . change the slope"
#define RAD  "+/- . . . . change radius"
#define SP   "SPACE . . . stop interactive"

static void	ft_print_obj_modif_keys(t_rtv1 *data, unsigned int col)
{
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 2, col, OBJ);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 20, col, UP);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 35, col, DOWN);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 50, col, LEFT);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 65, col, RGHT);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 80, col, ALT);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 95, col, ROT);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 110, col, RAD);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 295, 125, col, SP);
}

void		ft_print_controls_menu(t_rtv1 *data)
{
	unsigned int	color;

	color = 0x00bdbdbd;
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 2, color, CON);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 20, color, W);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 35, color, S);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 50, color, D);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 65, color, A);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 80, color, R_Z);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 95, color, R_Y);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 110, color, R_X);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 125, color, C_V);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 140, color, O);
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, 155, color, P);
	if (data->selected_obj)
		ft_print_obj_modif_keys(data, color);
}
