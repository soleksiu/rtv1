/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_dot_product.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 15:30:11 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/25 15:32:37 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

double	ft_vector_dot_product(t_vect *vector1, t_vect *vector2)
{
	double	result;

	result = vector1->x * vector2->x +
	vector1->y * vector2->y + vector1->z * vector2->z;
	return (result);
}
