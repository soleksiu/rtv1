/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_light_source.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 14:24:13 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/20 14:24:14 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_get_src_coords(char *line, t_light *light_source)
{
	char		*coords_pt;
	char		*tmp;

	coords_pt = NULL;
	if (!(coords_pt = ft_strstr(line, "c(")))
		return (0);
	light_source->pos.x = ft_atoi(coords_pt += 2);
	ft_move_ptr_while_nbr(&coords_pt);
	light_source->pos.y = ft_atoi(coords_pt += 2);
	ft_move_ptr_while_nbr(&coords_pt);
	light_source->pos.z = ft_atoi(coords_pt += 2);
	if ((tmp = ft_strstr(line, "move")))
	{
		light_source->pos.x += ft_atoi(tmp += 5);
		ft_move_ptr_while_nbr(&tmp);
		light_source->pos.y += ft_atoi(tmp += 2);
		ft_move_ptr_while_nbr(&tmp);
		light_source->pos.z += ft_atoi(tmp += 2);
	}
	return (1);
}

static int	ft_parse_source_params(char *line, t_light *light_source)
{
	char		*intens_pt;

	if (!(ft_get_src_coords(line, light_source)))
		return (0);
	intens_pt = ft_strstr(line, "i(");
	if (intens_pt)
	{
		light_source->intens = ft_atoi(intens_pt += 2) / 100.0;
		if (light_source->intens < 0)
			return (0);
	}
	else
		return (0);
	return (1);
}

static int	ft_parse_object_params(char *line, t_objct *obj_addr, t_rtv1 *data)
{
	char		*color_pt;

	if (!(color_pt = ft_strstr(line, "0x")))
		return (0);
	obj_addr->color.color = ft_parse_color(color_pt, data);
	if (!(ft_parse_source_params(line, obj_addr->addr)) ||
		!(obj_addr->color.color))
		return (0);
	return (1);
}

void		ft_create_light_source(char *line, t_objct *obj_addr, t_rtv1 *data)
{
	while (*line != '{')
		line++;
	obj_addr->type = light;
	if (!(obj_addr->addr = (void *)ft_memalloc(sizeof(t_light))))
		ft_error("error with memory allocation for light source", data);
	if (!(ft_parse_object_params(line, obj_addr, data)))
	{
		free(obj_addr->addr);
		ft_error("wrong light source parameters", data);
	}
}
