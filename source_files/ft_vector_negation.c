/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_negation.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 13:47:13 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/25 13:49:38 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vect	ft_vector_negation(t_vect *vector)
{
	t_vect	result;

	result.x = vector->x ? -vector->x : 0;
	result.y = vector->y ? -vector->y : 0;
	result.z = vector->z ? -vector->z : 0;
	return (result);
}
