/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calc_light.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 18:01:23 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/19 18:01:30 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

/*
** p - is point of intersection of the ray and object
** n - is a normalised light vector at this point
** light - is a coordinates of point light
*/

static int		ft_in_shadow(t_thread *data, t_vect *light_dir, t_vect *src_pos)
{
	double			t_min;
	t_objct			*obj_ptr;
	unsigned int	i;
	double			t;
	t_ray			ray;

	if (data->shadows == 0)
		return (0);
	ray.start = *src_pos;
	ray.direction = ft_vector_negation(light_dir);
	ray.direction = ft_vector_normalizing(&ray.direction);
	t_min = ft_vector_length(light_dir);
	t = DBL_MAX;
	i = 0;
	while (i < data->objcts_nbr)
	{
		t = DBL_MAX;
		obj_ptr = ((t_objct **)data->scene)[i];
		if (obj_ptr->ft_intersect(obj_ptr, ray, &t) &&
			(t > 0.01 && t < t_min - 0.1 && t != NAN))
			return (1);
		i++;
	}
	return (0);
}

static double	ft_calc_spec(t_thread *data, t_vect *l, double sp, double s_int)
{
	t_vect	r;
	double	r_dot_v;
	double	intens;
	t_vect	v;
	double	n_dot_l;

	intens = 0;
	n_dot_l = ft_vector_dot_product(&data->n, l);
	r = ft_vector_mult_scalar(&data->n, 2 * n_dot_l);
	r = ft_vector_subtraction(&r, l);
	v = ft_vector_negation(&data->ray.direction);
	r_dot_v = ft_vector_dot_product(&r, &v);
	if (r_dot_v > 0)
		intens = s_int * pow((r_dot_v /
		(ft_vector_length(&r) * ft_vector_length(&v))), sp);
	return (intens);
}

static double	ft_c_l(t_thread *data, int sp)
{
	unsigned int	i;
	double			intens;
	t_vect			l;
	t_light			*src_ptr;
	double			n_dot_l;

	i = 0;
	intens = data->ambient_light;
	while (i < data->src_nbr)
	{
		src_ptr = (t_light *)(((t_objct*)(data->light_srcs[i++]))->addr);
		l = ft_vector_subtraction(&src_ptr->pos, &data->p);
		n_dot_l = ft_vector_dot_product(&data->n, &l);
		if ((ft_in_shadow(data, &l, &src_ptr->pos)) && n_dot_l > 0)
			continue;
		if (n_dot_l > 0)
			intens += src_ptr->intens * n_dot_l /
			(ft_vector_length(&data->n) * ft_vector_length(&l));
		if (data->shine && sp)
			intens += ft_calc_spec(data, &l, sp, src_ptr->intens);
	}
	return (intens);
}

double			ft_calc_light(t_thread *data, t_objct *obj)
{
	double		coef;
	double		n_dot_d;
	t_vect		d;

	data->p = ft_find_hit_point(&data->ray, data->t);
	data->n = ft_find_normal(obj, &data->p, &data->ray, data->t);
	d = ft_vector_negation(&data->ray.direction);
	n_dot_d = ft_vector_dot_product(&data->n, &d);
	if (n_dot_d < 0)
		return (data->ambient_light);
	coef = ft_c_l(data, obj->spec);
	if (coef > 1)
		coef = 1;
	if (obj->selected)
		coef *= 0.80;
	return (coef);
}
