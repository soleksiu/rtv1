/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_normal.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 16:58:55 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/19 16:58:59 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static t_vect	ft_find_sphere_normal(t_vect *hit_point, t_sphere *sphere)
{
	t_vect	n;

	n = ft_vector_subtraction(hit_point, &sphere->center);
	n = ft_vector_normalizing(&n);
	return (n);
}

static t_vect	ft_find_cone_nrm(t_vect *p, t_cone *cone, t_ray *ray, double t)
{
	t_vect	tmp;
	t_vect	n;
	double	m;
	t_vect	x;
	double	k;

	k = tan(cone->angle * ANGLE_TO_RAD);
	x = ft_vector_subtraction(&ray->start, &cone->center);
	tmp = ft_vector_mult_scalar(&cone->v, t);
	m = ft_vector_dot_product(&ray->direction, &tmp) +
	ft_vector_dot_product(&x, &cone->v);
	tmp = ft_vector_mult_scalar(&cone->v, m * (1 + k * k));
	n = ft_vector_subtraction(p, &cone->center);
	tmp = ft_vector_subtraction(&n, &tmp);
	n = ft_vector_normalizing(&tmp);
	return (n);
}

static t_vect	ft_find_cyl_nrm(t_vect *p, t_cylinder *cyl, t_ray *r, double t)
{
	t_vect	tmp;
	t_vect	n;
	double	m;
	t_vect	x;

	x = ft_vector_subtraction(&r->start, &cyl->center);
	tmp = ft_vector_mult_scalar(&cyl->v, t);
	m = ft_vector_dot_product(&r->direction, &tmp) +
	ft_vector_dot_product(&x, &cyl->v);
	tmp = ft_vector_mult_scalar(&cyl->v, m);
	n = ft_vector_subtraction(p, &cyl->center);
	tmp = ft_vector_subtraction(&n, &tmp);
	n = ft_vector_normalizing(&tmp);
	return (n);
}

static t_vect	ft_find_plane_normal(t_ray *ray, t_plane *plane)
{
	t_vect	n;

	if (ray)
		n.z = 0;
	n = plane->normal;
	return (n);
}

t_vect			ft_find_normal(t_objct *o, t_vect *p, t_ray *r, double t)
{
	t_vect normal;

	normal = (t_vect){0, 0, 0};
	if (o->type == sphere)
		normal = ft_find_sphere_normal(p, o->addr);
	else if (o->type == cone)
		normal = ft_find_cone_nrm(p, o->addr, r, t);
	else if (o->type == cylinder)
		normal = ft_find_cyl_nrm(p, o->addr, r, t);
	else if (o->type == plane)
		normal = ft_find_plane_normal(r, o->addr);
	return (normal);
}
