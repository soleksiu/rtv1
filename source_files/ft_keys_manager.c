/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_keys_manager.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:09:01 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:09:05 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int		ft_redraw(void *d)
{
	t_rtv1		*data;

	data = (t_rtv1 *)d;
	ft_output_final_image(data);
	return (0);
}

static int		ft_exit_mouse(void *param)
{
	param = 0;
	exit(0);
	return (0);
}

static int		ft_check_key_code_mouse(int k_n, int x, int y, void *d)
{
	t_rtv1		*data;
	t_thread	d_tmp;
	t_coord		c;
	t_objct		*obj_ptr;
	t_objct		*old_selected;

	data = (t_rtv1 *)d;
	old_selected = (t_objct *)(data->selected_obj);
	if (k_n == 1)
	{
		c.x_b = x;
		c.y_b = y;
		ft_init_thread_params(data, &d_tmp, 0);
		if (!(obj_ptr = ft_intersect_objects(&d_tmp, &c)))
			return (1);
		obj_ptr->selected = 1;
		if (old_selected)
		{
			old_selected->selected = 0;
		}
		data->selected_obj = obj_ptr;
		mlx_clear_window(data->mlx_ptr, data->win_ptr);
		ft_draw_image(data);
	}
	return (1);
}

void			ft_keys_manager(t_rtv1 *data)
{
	mlx_hook(data->win_ptr, 17, 1L << 17, ft_exit_mouse, (void *)0);
	mlx_hook(data->win_ptr, 2, 5, ft_check_key_code, data);
	mlx_expose_hook(data->win_ptr, ft_redraw, data);
	mlx_mouse_hook(data->win_ptr, ft_check_key_code_mouse, data);
}
