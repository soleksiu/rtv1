/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_scene.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/29 17:44:40 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/29 17:45:51 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_set_cam(char *line, t_rtv1 *data)
{
	char *tmp;

	line += ft_strlen("camera");
	tmp = ft_strchr(line, 'c');
	data->cam.x = ft_atoi(tmp += 2);
	ft_move_ptr_while_nbr(&tmp);
	data->cam.y = ft_atoi(tmp += 2);
	ft_move_ptr_while_nbr(&tmp);
	data->cam.z = ft_atoi(tmp += 2);
}

static int	ft_get_line_values(char *line, t_objct *obj_ptr, t_rtv1 *data)
{
	int j;

	j = 0;
	line += ft_strlen("object ");
	if (ft_strstr(line, "cone"))
		ft_create_cone(line, obj_ptr, data);
	else if (ft_strstr(line, "sphere"))
		ft_create_sphere(line, obj_ptr, data);
	else if (ft_strstr(line, "cylinder"))
		ft_create_cylinder(line, obj_ptr, data);
	else if (ft_strstr(line, "plane"))
		ft_create_plane(line, obj_ptr, data);
	else
		ft_error("wrong object type in scene file", data);
	return (1);
}

static void	ft_get_additional_params(char *line, t_rtv1 *data)
{
	if (ft_strstr(line, "camera"))
		ft_set_cam(line, data);
	else if (ft_strstr(line, "shadows"))
		data->shadows = ft_strstr(line, "off") ? 0 : 1;
	else if (ft_strstr(line, "shine"))
		data->shine = ft_strstr(line, "off") ? 0 : 1;
	else if (ft_strstr(line, "ambient light"))
		data->ambient_light = ft_atoi((line) + 15) / 100.0;
}

static void	ft_create_addr_arr(t_rtv1 *data, unsigned int *i, unsigned int *l)
{
	if (!(data->scene = ft_memalloc(sizeof(t_objct *) * data->objcts_nbr)) ||
		!(data->light_srcs = ft_memalloc(sizeof(t_objct *) * data->src_nbr)))
		ft_error("not able to create the scene", data);
	while ((*i) < data->objcts_nbr)
		if (!(data->scene[(*i)++] = ft_memalloc(sizeof(t_objct))))
			ft_error("not able to create the scene", data);
	while ((*l) < data->src_nbr)
		if (!(data->light_srcs[(*l)++] = ft_memalloc(sizeof(t_objct))))
			ft_error("not able to create the scene", data);
}

void		ft_create_scene(t_rtv1 *data)
{
	unsigned int	i;
	unsigned int	l;
	t_list			*tmp_lst;

	i = 0;
	l = 0;
	ft_open_and_read_file(data);
	tmp_lst = data->lst;
	ft_create_addr_arr(data, &i, &l);
	while (tmp_lst)
	{
		if (ft_strstr(tmp_lst->content, "object"))
		{
			--i;
			if (!(ft_get_line_values(tmp_lst->content, data->scene[i], data)))
				ft_error("not able to create the scene", data);
		}
		else if (ft_strstr(tmp_lst->content, "light source"))
			ft_create_light_source(tmp_lst->content,
				data->light_srcs[--l], data);
		else
			ft_get_additional_params(tmp_lst->content, data);
		tmp_lst = tmp_lst->next;
	}
	ft_lstdel(&(data->lst), ft_del_link);
}
