/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_subtraction.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 14:09:27 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/25 14:09:58 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vect	ft_vector_subtraction(t_vect *vector1, t_vect *vector2)
{
	t_vect	result;

	result.x = vector1->x - vector2->x;
	result.y = vector1->y - vector2->y;
	result.z = vector1->z - vector2->z;
	return (result);
}
