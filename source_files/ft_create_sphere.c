/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_sphere.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 14:23:41 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/20 14:23:42 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_get_center_coords(char *line, t_sphere *sphere)
{
	char		*coords_pt;
	char		*tmp;

	coords_pt = NULL;
	if (!(coords_pt = ft_strstr(line, "c(")))
		return (0);
	sphere->center.x = ft_atoi(coords_pt += 2);
	ft_move_ptr_while_nbr(&coords_pt);
	sphere->center.y = ft_atoi(coords_pt += 2);
	ft_move_ptr_while_nbr(&coords_pt);
	sphere->center.z = ft_atoi(coords_pt += 2);
	if ((tmp = ft_strstr(line, "move")))
	{
		sphere->center.x += ft_atoi(tmp += 5);
		ft_move_ptr_while_nbr(&tmp);
		sphere->center.y += ft_atoi(tmp += 2);
		ft_move_ptr_while_nbr(&tmp);
		sphere->center.z += ft_atoi(tmp += 2);
	}
	return (1);
}

static int	ft_parse_sphere_params(char *line, t_sphere *sphere)
{
	char		*radius_pt;

	if (!(ft_get_center_coords(line, sphere)))
		return (0);
	radius_pt = ft_strstr(line, "r(");
	if (radius_pt)
	{
		sphere->radius = ft_atoi(radius_pt += 2);
		if (sphere->radius <= 0)
			return (0);
	}
	else
		return (0);
	return (1);
}

static int	ft_parse_object_params(char *line, t_objct *obj_addr, t_rtv1 *data)
{
	char		*spec_pt;
	char		*color_pt;

	if (!(spec_pt = ft_strchr(line, 's')))
		return (0);
	if (!(color_pt = ft_strstr(line, "0x")))
		return (0);
	obj_addr->spec = ft_atoi(spec_pt += 2);
	obj_addr->color.color = ft_parse_color(color_pt, data);
	if (!(ft_parse_sphere_params(line, obj_addr->addr)) ||
		!(obj_addr->color.color) || obj_addr->spec < 0)
		return (0);
	return (1);
}

void		ft_create_sphere(char *line, t_objct *obj_addr, t_rtv1 *data)
{
	while (*line != '{')
		line++;
	obj_addr->type = sphere;
	if (!(obj_addr->addr = (void *)ft_memalloc(sizeof(t_sphere))))
		ft_error("error with memory allocation for sphere", data);
	obj_addr->ft_intersect = ft_intersect_sphere;
	if (!(ft_parse_object_params(line, obj_addr, data)))
	{
		free(obj_addr->addr);
		ft_error("wrong sphere parameters", data);
	}
}
