/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate_point.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 14:48:47 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/15 14:48:49 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_rotate_z_axis(t_vect *res, t_vect *tmp, double rot_z)
{
	res->x = tmp->x * cos((rot_z) * ANGLE_TO_RAD) -
	tmp->y * sin((rot_z) * ANGLE_TO_RAD);
	res->y = tmp->x * sin((rot_z) * ANGLE_TO_RAD) +
	tmp->y * cos((rot_z) * ANGLE_TO_RAD);
}

t_vect		ft_rotate_point(t_vect *p, double rot_x, double rot_y, double rot_z)
{
	t_vect	res;
	t_vect	tmp;

	res = *p;
	if (rot_x)
	{
		res.y = p->y * cos(rot_x * ANGLE_TO_RAD) +
		p->z * sin(rot_x * ANGLE_TO_RAD);
		res.z = -p->y * sin(rot_x * ANGLE_TO_RAD) +
		p->z * cos(rot_x * ANGLE_TO_RAD);
	}
	tmp = rot_x ? res : *p;
	if (rot_y)
	{
		res.x = tmp.x * cos(rot_y * ANGLE_TO_RAD) +
		tmp.z * sin(rot_y * ANGLE_TO_RAD);
		res.z = -tmp.x * sin(rot_y * ANGLE_TO_RAD) +
		tmp.z * cos(rot_y * ANGLE_TO_RAD);
	}
	if (rot_x || rot_y)
		tmp = res;
	if (rot_z)
		ft_rotate_z_axis(&res, &tmp, rot_z);
	return (res);
}
