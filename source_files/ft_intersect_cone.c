/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intersect_cone.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 12:44:23 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/24 12:44:24 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_cut_cone(t_cone *cone, t_ray ray, t_vect *x, double *t)
{
	double	m;
	t_vect	v_t;

	v_t = ft_vector_mult_scalar(&cone->v, *t);
	m = ft_vector_dot_product(&ray.direction, &v_t) +
	ft_vector_dot_product(x, &cone->v);
	if (m > 0)
		return (0);
	return (1);
}

static int	ft_check_roots(t_roots *roots, double *t)
{
	double tmp;

	if (roots->r1 == NAN && roots->r2 == NAN)
		return (0);
	else if (roots->r1 == NAN)
		tmp = roots->r2;
	else if (roots->r2 == NAN)
		tmp = roots->r1;
	else
	{
		if (roots->r1 < 0)
			tmp = roots->r2;
		else if (roots->r2 < 0)
			tmp = roots->r1;
		else
			tmp = roots->r1 < roots->r2 ? roots->r1 : roots->r2;
	}
	if (tmp < 0)
		return (0);
	*t = tmp;
	return (1);
}

static void	ft_change_cone_params(t_objct *obj, t_cone *cone)
{
	cone->center.x += obj->dx;
	if (obj->dy)
	{
		cone->center.y += obj->dy;
	}
	cone->center.z += obj->dz;
	cone->angle += obj->radius_change;
	if (cone->angle < 1)
		cone->angle = 1;
	if (cone->angle > 89)
		cone->angle = 89;
	if (obj->rot_z)
	{
		cone->v = ft_rotate_point(&cone->v, 0, 0, obj->rot_z);
		cone->v = ft_vector_normalizing(&cone->v);
	}
	obj->dx = 0;
	obj->dy = 0;
	obj->dz = 0;
	obj->rot_z = 0;
	obj->radius_change = 0;
}

int			ft_intersect_cone(void *c, t_ray ray, double *t)
{
	double	k;
	t_cone	*cone;
	t_vect	x;
	t_coef	coef;
	t_roots	roots;

	cone = (t_cone *)(((t_objct*)c)->addr);
	if (((t_objct*)c)->selected)
		ft_change_cone_params(c, cone);
	k = tan(cone->angle * ANGLE_TO_RAD);
	k = 1 + k * k;
	x = ft_vector_subtraction(&ray.start, &cone->center);
	coef.a = ft_vector_dot_product(&ray.direction, &ray.direction) -
	k * pow(ft_vector_dot_product(&ray.direction, &cone->v), 2);
	coef.b = 2 * (ft_vector_dot_product(&ray.direction, &x) -
	(k * ft_vector_dot_product(&ray.direction, &cone->v) *
	ft_vector_dot_product(&x, &cone->v)));
	coef.c = ft_vector_dot_product(&x, &x) -
	(k * pow(ft_vector_dot_product(&x, &cone->v), 2));
	roots = ft_solve_quadratic_equation(coef.a, coef.b, coef.c);
	if (!ft_check_roots(&roots, t))
		return (0);
	if (cone->cut && ft_cut_cone(cone, ray, &x, t))
		return (0);
	return (1);
}
