/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_normalizing.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 15:11:43 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/25 15:17:12 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_vect	ft_vector_normalizing(t_vect *vector)
{
	t_vect	result;
	double	magnitude;

	magnitude = ft_vector_length(vector);
	result.x = 0;
	result.y = 0;
	result.z = 0;
	if (magnitude)
	{
		result.x = vector->x / magnitude;
		result.y = vector->y / magnitude;
		result.z = vector->z / magnitude;
	}
	return (result);
}
