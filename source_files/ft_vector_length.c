/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_length.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 13:19:58 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/25 13:20:01 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

double	ft_vector_length(t_vect *vector)
{
	double	result;

	result = 0;
	if (vector->x || vector->y || vector->z)
		result = sqrt(vector->x * vector->x + vector->y * vector->y +
			vector->z * vector->z);
	return (result);
}
