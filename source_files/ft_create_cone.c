/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_cone.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 14:23:27 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/20 14:23:28 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_get_vertex_coords(char *line, t_cone *cone)
{
	char		*coords_pt;
	char		*tmp;

	coords_pt = NULL;
	if (!(coords_pt = ft_strstr(line, "c(")))
		return (0);
	cone->center.x = ft_atoi(coords_pt += 2);
	ft_move_ptr_while_nbr(&coords_pt);
	cone->center.y = ft_atoi(coords_pt += 2);
	ft_move_ptr_while_nbr(&coords_pt);
	cone->center.z = ft_atoi(coords_pt += 2);
	if ((tmp = ft_strstr(line, "move")))
	{
		cone->center.x += ft_atoi(tmp += 5);
		ft_move_ptr_while_nbr(&tmp);
		cone->center.y += ft_atoi(tmp += 2);
		ft_move_ptr_while_nbr(&tmp);
		cone->center.z += ft_atoi(tmp += 2);
	}
	return (1);
}

static int	ft_get_normal(char *line, t_cone *cone)
{
	char		*normal_pt;
	char		*rotate_pt;
	double		rot_x;
	double		rot_y;
	double		rot_z;

	if (!(normal_pt = ft_strstr(line, "v(")))
		return (0);
	cone->v.x = ft_atoi(normal_pt += 2);
	ft_move_ptr_while_nbr(&normal_pt);
	cone->v.y = ft_atoi(normal_pt += 2);
	ft_move_ptr_while_nbr(&normal_pt);
	cone->v.z = ft_atoi(normal_pt += 2);
	ft_move_ptr_while_nbr(&normal_pt);
	if ((rotate_pt = ft_strstr(line, "rotate")))
	{
		rot_x = ft_atoi(rotate_pt += 7) * (-1.0);
		ft_move_ptr_while_nbr(&rotate_pt);
		rot_y = ft_atoi(rotate_pt += 2) * (-1.0);
		ft_move_ptr_while_nbr(&rotate_pt);
		rot_z = ft_atoi(rotate_pt += 2) * (-1.0);
		cone->v = ft_rotate_point(&cone->v, rot_x, rot_y, rot_z);
		cone->v = ft_vector_normalizing(&cone->v);
	}
	return (1);
}

static int	ft_parse_cone_params(char *line, t_cone *cone)
{
	char		*angle_pt;

	if (!(ft_get_vertex_coords(line, cone)) || !(ft_get_normal(line, cone)))
		return (0);
	angle_pt = ft_strstr(line, "a(");
	if (angle_pt)
	{
		cone->angle = ft_atoi(angle_pt += 2);
		if (cone->angle <= 0)
			return (0);
	}
	else
		return (0);
	if (ft_strstr(line, "t(cutted)"))
		cone->cut = 1;
	return (1);
}

static int	ft_parse_object_params(char *line, t_objct *obj_addr, t_rtv1 *data)
{
	char		*spec_pt;
	char		*color_pt;

	if (!(spec_pt = ft_strchr(line, 's')))
		return (0);
	if (!(color_pt = ft_strstr(line, "0x")))
		return (0);
	obj_addr->spec = ft_atoi(spec_pt += 2);
	obj_addr->color.color = ft_parse_color(color_pt, data);
	if (!(ft_parse_cone_params(line, obj_addr->addr)) ||
		!(obj_addr->color.color) || obj_addr->spec < 0)
		return (0);
	return (1);
}

void		ft_create_cone(char *line, t_objct *obj_addr, t_rtv1 *data)
{
	while (*line != '{')
		line++;
	obj_addr->type = cone;
	if (!(obj_addr->addr = (void *)ft_memalloc(sizeof(t_cone))))
		ft_error("error with memory allocation for cone", data);
	obj_addr->ft_intersect = ft_intersect_cone;
	if (!(ft_parse_object_params(line, obj_addr, data)))
	{
		free(obj_addr->addr);
		ft_error("wrong cone parameters", data);
	}
}
