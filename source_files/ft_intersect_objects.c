/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intersect_objects.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 19:50:17 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/16 19:50:18 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void		ft_set_ray_params(t_thread *data, t_coord *scr_crds)
{
	double x_global;
	double y_global;
	t_vect o;
	t_vect d;
	t_vect c;

	o.x = data->cam.x;
	o.y = data->cam.y;
	o.z = data->cam.z + data->zoom;
	o = ft_rotate_point(&o, data->rotate_x, data->rotate_y, data->rotate_z);
	o.x += data->move_x;
	o.y += data->move_y;
	data->ray.start = o;
	x_global = scr_crds->x_b;
	y_global = scr_crds->y_b;
	ft_convert_coords(&x_global, &y_global,
		data->window_width, data->window_height);
	c.x = x_global + data->move_x;
	c.y = y_global + data->move_y;
	c.z = -1 + data->zoom;
	c = ft_rotate_point(&c, data->rotate_x, data->rotate_y, data->rotate_z);
	d = ft_vector_subtraction(&c, &o);
	data->ray.direction = ft_vector_normalizing(&d);
}

t_objct			*ft_intersect_objects(t_thread *data, t_coord *c)
{
	t_objct			*obj_ptr;
	t_objct			*res_obj_ptr;
	unsigned int	i;
	double			t;
	double			t_min;

	ft_set_ray_params(data, c);
	t = DBL_MAX;
	t_min = DBL_MAX;
	i = 0;
	res_obj_ptr = NULL;
	while (i < data->objcts_nbr)
	{
		t = DBL_MAX;
		obj_ptr = ((t_objct **)data->scene)[i];
		if (obj_ptr->ft_intersect(obj_ptr, data->ray, &t) && t < t_min)
		{
			t_min = t;
			res_obj_ptr = obj_ptr;
		}
		i++;
	}
	if (res_obj_ptr)
		data->t = t_min;
	return (res_obj_ptr);
}
