/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:05:50 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:05:52 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"
#define USAGE "usage : rtv1 scene_path\n"

void		ft_error(char *str, t_rtv1 *data)
{
	if (ft_strequ(str, "usage"))
		write(2, USAGE, ft_strlen(USAGE));
	else
	{
		write(2, "\033[1m""\x1B[31m""error: ""\x1B[37m", 21);
		write(2, str, ft_strlen(str));
		write(2, "\033[0m""\n", 5);
	}
	if (data && data->mlx_ptr && data->win_ptr)
		mlx_destroy_window(data->mlx_ptr, data->win_ptr);
	exit(-1);
}
