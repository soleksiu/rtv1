/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_and_read_file.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 18:40:28 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/20 18:40:29 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	ft_open_and_read_file(t_rtv1 *data)
{
	int		i;
	char	*check_42;

	i = 0;
	if ((data->fd = open(data->file_path, O_RDONLY)) == -1)
		ft_error("not able to open the file", data);
	check_42 = ft_strstr(data->file_path, ".42");
	if (!check_42 || check_42[3] != '\0')
		ft_error("wrong file format! only *.42 files are supported", data);
	while (ft_get_next_line(data->fd, &data->line) > 0)
	{
		if (data->line[0] != '#')
		{
			ft_lstadd(&data->lst, ft_lstnew(data->line,
			ft_strlen(data->line) + 1));
			if (ft_strstr(data->line, "object"))
				data->objcts_nbr++;
			else if (ft_strstr(data->line, "light source"))
				data->src_nbr++;
		}
		free(data->line);
	}
}
