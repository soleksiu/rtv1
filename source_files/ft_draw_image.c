/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_image.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:05:34 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:05:36 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_calculate_with_threads(t_rtv1 *data)
{
	int				thread_c;
	void			*thread_return;
	pthread_t		threads[THREADS_NBR];
	t_thread		th_data[THREADS_NBR];

	thread_c = 0;
	while (thread_c < THREADS_NBR)
	{
		ft_memset((void*)&th_data[thread_c], 0, sizeof(t_thread));
		ft_init_thread_params(data, &th_data[thread_c], thread_c);
		if (pthread_create(&threads[thread_c], NULL,
		(void *)ft_ray_trace, (void *)(&th_data[thread_c])) == -1)
			ft_error("Error with creating thread", data);
		thread_c++;
	}
	thread_c = -1;
	while (++thread_c < THREADS_NBR)
		if (pthread_join(threads[thread_c], &thread_return) == -1)
			ft_error("Error with joining thread", data);
}

void		ft_draw_image(t_rtv1 *data)
{
	ft_calculate_with_threads(data);
	ft_output_final_image(data);
}
