/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:10:09 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:10:14 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_init_starting_params(t_rtv1 *data)
{
	data->window_width = data->window_width == 0 ?
	M_WIDTH : data->window_width * 1;
	data->window_height = data->window_height == 0
	? M_HEIGHT : data->window_height * 1;
	data->ambient_light = 0.2;
	data->zoom = 1.0;
	data->shadows = 0;
	data->shine = 0;
}

static void	ft_init_keys(t_rtv1 *data)
{
	data->keys_arr[0] = KEY_A;
	data->keys_arr[1] = KEY_S;
	data->keys_arr[2] = KEY_D;
	data->keys_arr[3] = KEY_W;
	data->keys_arr[4] = KEY_Q;
	data->keys_arr[5] = KEY_E;
	data->keys_arr[6] = KEY_R;
	data->keys_arr[7] = KEY_M;
	data->keys_arr[8] = KEY_UP;
	data->keys_arr[9] = KEY_DOWN;
	data->keys_arr[10] = KEY_LEFT;
	data->keys_arr[11] = KEY_RIGHT;
	data->keys_arr[12] = KEY_PGUP;
	data->keys_arr[13] = KEY_PGDWN;
	data->keys_arr[14] = KEY_ESC;
	data->keys_arr[15] = KEY_SPACE;
	data->keys_arr[16] = KEY_PLUS;
	data->keys_arr[17] = KEY_MINUS;
	data->keys_arr[18] = KEY_LARR;
	data->keys_arr[19] = KEY_RARR;
	data->keys_arr[20] = KEY_HOME;
	data->keys_arr[21] = KEY_END;
	data->keys_arr[22] = KEY_C;
	data->keys_arr[23] = KEY_V;
	data->keys_arr[24] = KEY_L;
}

static void	ft_init_mlx(t_rtv1 *data)
{
	data->mlx_ptr = mlx_init();
	data->win_ptr = mlx_new_window(data->mlx_ptr, data->window_width,
	data->window_height, "soleksiu_RTv1");
	if (!data->img_ptr)
	{
		data->img_ptr = mlx_new_image(data->mlx_ptr,
		data->window_width, data->window_height);
		data->img_addr = mlx_get_data_addr(data->img_ptr,
		&data->img_bpp, &data->img_size_line, &data->img_endian);
	}
}

int			main(int argc, char **argv)
{
	int		keys[KEYS_NBR];
	t_rtv1	rtv1_data;
	t_rtv1	*data;

	data = &rtv1_data;
	ft_memset((void *)data, 0, sizeof(rtv1_data));
	if (argc != 2)
		ft_error("usage", data);
	data->file_path = argv[1];
	data->keys_arr = &keys[0];
	ft_init_starting_params(data);
	ft_init_mlx(data);
	ft_init_keys(data);
	data->keys_arr[25] = KEY_K;
	data->keys_arr[26] = KEY_O;
	data->keys_arr[27] = KEY_P;
	ft_create_scene(data);
	ft_draw_image(data);
	ft_keys_manager(data);
	mlx_loop(data->mlx_ptr);
	return (0);
}
