/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_plane.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 14:23:15 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/20 14:23:17 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_get_vertex_coords(char *line, t_plane *plane)
{
	char		*coords_pt;
	char		*tmp;

	coords_pt = NULL;
	if (!(coords_pt = ft_strstr(line, "c(")))
		return (0);
	plane->center.x = ft_atoi(coords_pt += 2);
	ft_move_ptr_while_nbr(&coords_pt);
	plane->center.y = ft_atoi(coords_pt += 2);
	ft_move_ptr_while_nbr(&coords_pt);
	plane->center.z = ft_atoi(coords_pt += 2);
	if ((tmp = ft_strstr(line, "move")))
	{
		plane->center.x += ft_atoi(tmp += 5);
		ft_move_ptr_while_nbr(&tmp);
		plane->center.y += ft_atoi(tmp += 2);
		ft_move_ptr_while_nbr(&tmp);
		plane->center.z += ft_atoi(tmp += 2);
	}
	return (1);
}

static int	ft_get_normal(char *line, t_plane *plane)
{
	char		*normal_pt;
	char		*tmp;
	double		rot_x;
	double		rot_y;
	double		rot_z;

	if (!(normal_pt = ft_strstr(line, "n(")))
		return (0);
	plane->normal.x = ft_atoi(normal_pt += 2);
	ft_move_ptr_while_nbr(&normal_pt);
	plane->normal.y = ft_atoi(normal_pt += 2);
	ft_move_ptr_while_nbr(&normal_pt);
	plane->normal.z = ft_atoi(normal_pt += 2);
	ft_move_ptr_while_nbr(&normal_pt);
	if ((tmp = ft_strstr(line, "rotate")))
	{
		rot_x = ft_atoi(tmp += 7) * (-1.0);
		ft_move_ptr_while_nbr(&tmp);
		rot_y = ft_atoi(tmp += 2) * (-1.0);
		ft_move_ptr_while_nbr(&tmp);
		rot_z = ft_atoi(tmp += 2) * (-1.0);
		plane->normal = ft_rotate_point(&plane->normal, rot_x, rot_y, rot_z);
		plane->normal = ft_vector_normalizing(&plane->normal);
	}
	return (1);
}

static int	ft_parse_plane_params(char *line, t_plane *plane)
{
	if (!(ft_get_vertex_coords(line, plane)) || !(ft_get_normal(line, plane)))
		return (0);
	return (1);
}

static int	ft_parse_object_params(char *line, t_objct *obj_addr, t_rtv1 *data)
{
	char		*spec_pt;
	char		*color_pt;

	if (!(spec_pt = ft_strchr(line, 's')))
		return (0);
	if (!(color_pt = ft_strstr(line, "0x")))
		return (0);
	obj_addr->spec = ft_atoi(spec_pt += 2);
	obj_addr->color.color = ft_parse_color(color_pt, data);
	if (!(ft_parse_plane_params(line, obj_addr->addr)) ||
		!(obj_addr->color.color) || obj_addr->spec < 0)
		return (0);
	return (1);
}

void		ft_create_plane(char *line, t_objct *obj_addr, t_rtv1 *data)
{
	while (*line != '{')
		line++;
	obj_addr->type = plane;
	if (!(obj_addr->addr = (void *)ft_memalloc(sizeof(t_plane))))
		ft_error("error with memory allocation for plane", data);
	obj_addr->ft_intersect = ft_intersect_plane;
	if (!(ft_parse_object_params(line, obj_addr, data)))
	{
		free(obj_addr->addr);
		ft_error("wrong plane parameters", data);
	}
}
