/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intersect_cylinder.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 12:44:42 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/24 12:44:44 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int	ft_check_roots(t_roots *roots, double *t)
{
	double tmp;

	if (roots->r1 == NAN && roots->r2 == NAN)
		return (0);
	else if (roots->r1 == NAN)
		tmp = roots->r2;
	else if (roots->r2 == NAN)
		tmp = roots->r1;
	else
	{
		if (roots->r1 < 0)
			tmp = roots->r2;
		else if (roots->r2 < 0)
			tmp = roots->r1;
		else
			tmp = roots->r1 < roots->r2 ? roots->r1 : roots->r2;
	}
	if (tmp < 0)
		return (0);
	*t = tmp;
	return (1);
}

static void	ft_change_cylinder_params(t_objct *obj, t_cylinder *cylinder)
{
	cylinder->center.x += obj->dx;
	cylinder->center.z += obj->dz;
	cylinder->radius += obj->radius_change;
	if (cylinder->radius < 1)
		cylinder->radius = 1;
	if (obj->rot_z)
	{
		cylinder->v = ft_rotate_point(&cylinder->v, 0, 0, obj->rot_z);
		cylinder->v = ft_vector_normalizing(&cylinder->v);
	}
	obj->dx = 0;
	obj->dz = 0;
	obj->radius_change = 0;
	obj->rot_z = 0;
}

int			ft_intersect_cylinder(void *c, t_ray ray, double *t)
{
	t_cylinder	*cylinder;
	t_vect		x;
	t_coef		coef;
	t_roots		roots;

	cylinder = (t_cylinder *)(((t_objct*)c)->addr);
	if (((t_objct*)c)->selected)
		ft_change_cylinder_params(c, cylinder);
	x = ft_vector_subtraction(&ray.start, &cylinder->center);
	coef.a = ft_vector_dot_product(&ray.direction, &ray.direction) -
	pow(ft_vector_dot_product(&ray.direction, &cylinder->v), 2);
	coef.b = 2 * (ft_vector_dot_product(&ray.direction, &x) -
	(ft_vector_dot_product(&ray.direction, &cylinder->v) *
	ft_vector_dot_product(&x, &cylinder->v)));
	coef.c = ft_vector_dot_product(&x, &x) -
	(pow(ft_vector_dot_product(&x, &cylinder->v), 2)) -
	(cylinder->radius * cylinder->radius);
	roots = ft_solve_quadratic_equation(coef.a, coef.b, coef.c);
	if (!ft_check_roots(&roots, t))
		return (0);
	return (1);
}
