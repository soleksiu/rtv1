/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_color.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 14:55:29 by soleksiu          #+#    #+#             */
/*   Updated: 2018/05/03 14:55:31 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static int			ft_check_values(char c)
{
	return (((c >= '0' && c <= '9') ||
		(ft_tolower(c) >= 'a' && ft_tolower(c) <= 'f')));
}

unsigned int		ft_parse_color(char *line, t_rtv1 *data)
{
	int			color_value;
	int			r;
	int			i;

	i = 0;
	color_value = 0;
	if (*line != '0' || *(line + 1) != 'x')
		ft_error("error: wrong color input", data);
	line += 2;
	while (*line && *line != ' ')
	{
		if (*line == '}' || *line == ' ')
			break ;
		if (!ft_check_values(*line))
			ft_error("error: wrong color input", data);
		if (ft_isdigit(*line))
			r = *line - '0';
		else
			r = 10 + ft_tolower(*line) - 'a';
		color_value *= 16;
		color_value += r;
		(line)++;
	}
	return (color_value);
}
