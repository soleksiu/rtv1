/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solve_quadratic_equation.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 16:36:07 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/25 16:53:42 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_roots	ft_solve_quadratic_equation(double a, double b, double c)
{
	double	discriminant;
	t_roots	result;

	discriminant = b * b - (4 * a * c);
	if (discriminant < 0 || (!a && !b))
	{
		result.r1 = NAN;
		result.r2 = NAN;
	}
	else if (a == 0 && b)
	{
		result.r1 = -c / b;
		result.r2 = NAN;
	}
	else if (discriminant == 0)
	{
		result.r1 = -b / (2 * a);
		result.r2 = NAN;
	}
	else
	{
		result.r1 = (-b + sqrt(discriminant)) / (2 * a);
		result.r2 = (-b - sqrt(discriminant)) / (2 * a);
	}
	return (result);
}
