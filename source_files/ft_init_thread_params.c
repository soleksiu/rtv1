/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_thread_params.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 20:03:27 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/16 20:03:28 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void	ft_calc_thread_coords_diapason(t_rtv1 *data, t_thread *th_dt, int c)
{
	int	dy;

	dy = data->window_height / THREADS_NBR;
	th_dt->begin_x = 0;
	th_dt->begin_y = dy * c;
	if (dy * THREADS_NBR != data->window_height && c == THREADS_NBR - 1)
		dy = dy + (data->window_height - dy * THREADS_NBR);
	th_dt->dx = data->window_width;
	th_dt->dy = dy;
}

void		ft_init_thread_params(t_rtv1 *data, t_thread *th_data, int c)
{
	ft_calc_thread_coords_diapason(data, th_data, c);
	th_data->color = data->default_color;
	th_data->window_width = data->window_width;
	th_data->window_height = data->window_height;
	th_data->img_bpp = data->img_bpp;
	th_data->img_size_line = data->img_size_line;
	th_data->img_endian = data->img_endian;
	th_data->img_addr = data->img_addr;
	th_data->zoom = data->zoom;
	th_data->scene = data->scene;
	th_data->light_srcs = data->light_srcs;
	th_data->objcts_nbr = data->objcts_nbr;
	th_data->src_nbr = data->src_nbr;
	th_data->move_x = data->move_x;
	th_data->move_y = data->move_y;
	th_data->cam = data->cam;
	th_data->ambient_light = data->ambient_light;
	th_data->rotate_x = data->rotate_x;
	th_data->rotate_y = data->rotate_y;
	th_data->rotate_z = data->rotate_z;
	th_data->shadows = data->shadows;
	th_data->shine = data->shine;
}
