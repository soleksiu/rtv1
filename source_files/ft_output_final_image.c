/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output_final_image.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/23 12:26:03 by soleksiu          #+#    #+#             */
/*   Updated: 2018/08/23 12:26:05 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	ft_output_final_image(t_rtv1 *data)
{
	mlx_put_image_to_window(data->mlx_ptr, data->win_ptr, data->img_ptr, 0, 0);
	mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 209,
	M_HEIGHT - 25, 0x0000cdcd, "press 'ESC' for exit");
	if (data->menu == 0)
		mlx_string_put(data->mlx_ptr, data->win_ptr, M_WIDTH - 319,
			2, 0x0000cdcd, "press 'm' to show controls menu");
	mlx_string_put(data->mlx_ptr, data->win_ptr, 8, M_HEIGHT - 25,
		0x0000cdcd, "press 'r' to recover camera position");
	data->menu == 1 ? ft_print_controls_menu(data) : 0;
}
