/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_color.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:08:44 by soleksiu          #+#    #+#             */
/*   Updated: 2018/07/08 16:08:49 by soleksiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

unsigned int		ft_get_color(unsigned int color, double coef)
{
	t_color	c;

	c.color = color;
	c.rgb[red] = (unsigned char)(c.rgb[red] * coef);
	c.rgb[green] = (unsigned char)(c.rgb[green] * coef);
	c.rgb[blue] = (unsigned char)(c.rgb[blue] * coef);
	return (c.color);
}
