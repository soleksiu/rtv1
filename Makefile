#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/08 16:02:30 by soleksiu          #+#    #+#              #
#    Updated: 2018/07/08 16:02:35 by soleksiu         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

SRCDIR = source_files

OBJDIR = object_files

SRC = main.c ft_create_scene.c ft_open_and_read_file.c ft_create_light_source.c \
	ft_create_plane.c ft_create_sphere.c ft_create_cylinder.c ft_create_cone.c \
	ft_draw_image.c ft_put_pixel_to_img.c\
	ft_convert_coords.c ft_rotate_point.c ft_parse_color.c ft_get_color.c \
	ft_solve_quadratic_equation.c ft_ray_trace.c ft_calc_light.c \
	ft_vector_length.c ft_vector_negation.c ft_vector_addition.c ft_vector_subtraction.c \
	ft_vector_mult_scalar.c ft_vector_normalizing.c ft_find_normal.c\
	ft_vector_dot_product.c ft_vector_cross_product.c ft_find_hit_point.c\
	ft_intersect_cone.c ft_intersect_cylinder.c ft_intersect_sphere.c ft_intersect_plane.c \
   	ft_keys_manager.c ft_check_key_code.c ft_print_controls_menu.c \
   	ft_intersect_objects.c ft_init_thread_params.c ft_error.c ft_output_final_image.c
   	

OBJ = $(SRC:.c=.o)
OBJS = $(addprefix $(OBJDIR)/,$(SRC:.c=.o))

NAME = rtv1

WRN =  -Wall -Wextra -Werror

PTEST = ../test_rtv1/

all: objdir $(NAME)

$(NAME): objdir lib $(OBJS)
	@gcc $(WRN) $(OBJS) -L ./libft -lft -L ./lib_mlx -lmlx -framework OpenGL -framework AppKit -o $(NAME) 

lib:
	@make -C ./libft/

objdir: 
	@mkdir -p $(OBJDIR)

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	@gcc $(WRN) -I includes -I /usr/local/include -c $< -o $@  

test: all
	@./$(NAME) ${fractol}

backup: fclean
	@cp -r * ~/Desktop/backup_rtv1

clean:
	@rm -rf $(OBJ)
	@make clean -C ./libft

fclean: clean
	@find . -name "*~" -delete -or -name "#*#" -delete -or -name ".DS_Store" -delete -or -name "a.out" -delete
	@make fclean -C ./libft/
	@rm -rf $(NAME)
	@rm -rf $(OBJDIR)

re: fclean all